package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;

@RestController
public class RestControllerPeserta {
	@Autowired
	PesertaService pesertaDAO;
	
	@RequestMapping("rest/peserta")
	public PesertaModel peserta(Model model, @RequestParam(value = "nomor" , required = false) String nomor)
	{
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
		if(peserta != null){
			int umur = pesertaDAO.countUsia(peserta);
			model.addAttribute("peserta", peserta);
			model.addAttribute("umur", umur);
			return peserta;
		}
		else {
			model.addAttribute("nomor-peserta", nomor);
			return peserta;
		}
		
	}
}
