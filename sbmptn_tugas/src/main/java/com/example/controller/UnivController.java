package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.UnivModel;
import com.example.service.UnivService;

@Controller
public class UnivController {
	@Autowired
	UnivService univDAO;
	
	@RequestMapping("/univ")
	public String view(Model model)
	{
		List<UnivModel> univs = univDAO.selectAllUniv();
		model.addAttribute("univs", univs);
		return "viewalluniv";
	}
	
	
 	@RequestMapping("/univ/{kodeUniv}")
    public String viewUnivById (Model model, @PathVariable(value = "kodeUniv") String kode_univ)
    {
        UnivModel universitasProdi = univDAO.selectUniversitasAndProdi(kode_univ);
        if (universitasProdi != null) {
            model.addAttribute ("univs", universitasProdi);
            return "view-detail-universitas";
        } else {
            model.addAttribute ("kode_univ", kode_univ);
            return "univ_notfound";
        }
    }
}
