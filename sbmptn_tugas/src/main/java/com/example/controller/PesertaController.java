package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;

@Controller
public class PesertaController {
	@Autowired
	PesertaService pesertaDAO;
	
	@RequestMapping("/")
    public String index ()
    {
        return "home";
    }
	
	@RequestMapping("/pengumuman/submit")
	public String submitPengumuman (Model model, @RequestParam(value ="nomor", required = true) String nomor){
			PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
			if(peserta != null){
				model.addAttribute("peserta", peserta);
				return "info-pengumuman";
			}
			else {
				model.addAttribute("peserta", peserta );
				return "not-found";
			}
	}
	
	@RequestMapping("/peserta")
	public String getDetailProdi(@RequestParam(value = "nomor") String nomor_peserta, Model model) {
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor_peserta);
		if(peserta != null){
			int umur = pesertaDAO.countUsia(peserta);
			model.addAttribute("peserta", peserta);
			model.addAttribute("umur", umur);
			return "view-detail-peserta";
		}
		else {
			model.addAttribute("nomor-peserta", nomor_peserta);
			return "not-found";
		}
	}
}
