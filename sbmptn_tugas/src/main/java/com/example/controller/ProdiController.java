package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;

@Controller
public class ProdiController {
	@Autowired
	ProdiService prodiDAO;
	@Autowired
	PesertaService pesertaDAO;
	@RequestMapping("/prodi")
	public String getDetailProdi(@RequestParam(value = "kode") String kode_prodi, Model model) {
		ProdiModel prodiUnivPeserta = prodiDAO.selectProdi(kode_prodi);
        if (prodiUnivPeserta != null) {
        	PesertaModel peserta_maximum = prodiDAO.peserta_max(prodiUnivPeserta.getPeserta());
        	PesertaModel peserta_minimum = prodiDAO.peserta_min(prodiUnivPeserta.getPeserta());
        	
        	int umurMaxPeserta = pesertaDAO.countUsia(peserta_maximum);
        	int umurMinPeserta = pesertaDAO.countUsia(peserta_minimum);
        	
        	model.addAttribute ("maxPeserta", peserta_maximum);
        	model.addAttribute ("umurMaxPeserta", umurMaxPeserta);
        	model.addAttribute ("minPeserta", peserta_minimum);
        	model.addAttribute ("umurMinPeserta", umurMinPeserta);
            model.addAttribute ("prodiUnivPeserta", prodiUnivPeserta);
            return "view-prodi";
        } else {
            model.addAttribute ("kode_univ", kode_prodi);
            return "universitas-not-found";
        }
	}
}
