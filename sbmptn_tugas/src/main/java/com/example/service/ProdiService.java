package com.example.service;

import java.util.List;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;

public interface ProdiService {
	ProdiModel selectProdi (String kode_prodi);
	List<ProdiModel> selectAllProdi();
	void deleteProdi(String kode_prodi);
	PesertaModel peserta_max(List<PesertaModel> peserta);
	PesertaModel peserta_min(List<PesertaModel> peserta);
	int getUmur(PesertaModel peserta);
}
