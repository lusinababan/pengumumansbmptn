package com.example.service;

import java.util.List;

import com.example.model.UnivModel;

public interface UnivService {
	List<UnivModel> selectAllUniv();
	UnivModel selectUniversitasAndProdi (String kode_univ);
	void insertUniv (UnivModel univ);
	void deleteUniv (String kode_univ);
}
