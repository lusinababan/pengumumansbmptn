package com.example.service;

import com.example.model.PesertaModel;

public interface PesertaService {
	PesertaModel selectPeserta (String nomor);
	int countUsia(PesertaModel peserta);
	
}
