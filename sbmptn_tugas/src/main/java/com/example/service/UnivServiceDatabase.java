package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UnivMapper;
import com.example.model.UnivModel;


import groovy.util.logging.Slf4j;
@Slf4j
@Service
public class UnivServiceDatabase implements UnivService{

	@Autowired
	private UnivMapper univMapper;

	@Override
	public List<UnivModel> selectAllUniv() {
		// TODO Auto-generated method stub
		return univMapper.selectAllUniv();
	}

	/*@Override
	public UnivModel selectUniv(String kode_univ) {
		// TODO Auto-generated method stub
		return univMapper.selectUniv(kode_univ);
	}*/

	@Override
	public void insertUniv(UnivModel univ) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteUniv(String kode_univ) {
		// TODO Auto-generated method stub
		univMapper.deleteUniv(kode_univ);
	}

	@Override
	public UnivModel selectUniversitasAndProdi(String kode_univ) {
		// TODO Auto-generated method stub
		return univMapper.selectUniversitasAndProdi(kode_univ);
	
	}
	

}
